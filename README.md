# Reign Challenge

This repository contains technical test to apply as a full-stack developer

## Run
0. Install Docker and docker-compose.
1. Clone this repository using this command ```git clone --recurse-submodules https://gitlab.com/saespinosac/reign-challenge.git```.
2. Execute in folder `reign-challenge` ```docker-compose up``` to run project.
3. Go to http://localhost/

## Endpoints

* ``GET /hacker-news`` - `Get recent hacker news`
* ``DELETE /hacker-news/${id}`` - `Soft delete a hacker new`

## Considerations not fulfilled
* At least 30% test coverage (statements) for the server component.


## Extra information
* Automatically the server application populates the database with news.